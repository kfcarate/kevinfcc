//
//  GameModel.swift
//  GameGr1
//
//  Created by kevin on 15/11/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import Foundation
class GameModel {
    var puntaje:Int = 0
    var ronda:Int = 1
    var objetivo:Int?
    
    init() {
        //objetivo = setObjectivo()
        setObjectivo()
    }
    
    
    /*func setObjectivo() -> Int{
        return Int(arc4random_uniform(99) + 1)
    }*/
    
    func setObjectivo(){
            objetivo = Int(arc4random_uniform(99) + 1)
    }
    
    /*func jugar(){
        ronda += 1
    }*/
    
    func jugar(valorIntento: Int){
        ronda += 1
        puntaje += calcularPuntaje(valorIntento: valorIntento)
    }
    
    func reiniciar(){
        puntaje = 0
            ronda = 1
    }
    
    func calcularPuntaje(valorIntento:Int) -> Int{
        
           let diferencia = abs(valorIntento - objetivo!)
        
            switch diferencia {
                case 0:
                      return 100
                case 1...3:
                      return 75
                case 4...10:
                      return 50
                default:
                      return 0
        }
    }
}
