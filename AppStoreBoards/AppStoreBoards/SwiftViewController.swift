//
//  SwiftViewController.swift
//  AppStoreBoards
//
//  Created by kevin on 17/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit

class SwiftViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var customView: UIView!
    
    @IBAction func swipeUpAction(_ sender: Any) {
        
        let action = sender as! UISwipeGestureRecognizer
        
        
        customView.backgroundColor = .red
    }
    
    @IBAction func swipeDownAction(_ sender: Any) {
        customView.backgroundColor = .green
    }
    
    @IBAction func swipeLeftAction(_ sender: Any) {
        customView.backgroundColor = .yellow
    }
    @IBAction func swipeRightAction(_ sender: Any) {
        customView.backgroundColor = .orange
    }
    
}
