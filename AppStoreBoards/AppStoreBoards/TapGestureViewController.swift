//
//  TapGestureViewController.swift
//  AppStoreBoards
//
//  Created by kevin on 17/1/18.
//  Copyright © 2018 kevin. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    
    @IBOutlet weak var touchLabel: UILabel!
    
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var coordenadasLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchLabel.text = "\(touchCount ?? 0)"
        tapLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordenadasLabel.text = "x: \(x!), y: \(y!)"
        //print("x: \(x!), y: \(y!)")
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("terminó")
    }
    
    
    @IBAction func TapAction(_ sender: UITapGestureRecognizer) {
        
        customView.backgroundColor = .blue
        
    //    var taps = 0
        
//        if sender.state == .ended{
//            taps += 1
//            tapLabel.text = "\(taps)"
//        }
        
    }
    
    

}
