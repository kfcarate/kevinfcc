//
//  CiudadViewController.swift
//  App1
//
//  Created by kevin on 31/10/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    //MARK:- Outlets
    
    
    @IBOutlet weak var CuidadTextField: UITextField!
    
    @IBOutlet weak var ClimaLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let service = Servicios()
        service.consultaPorCiudad(city: CuidadTextField.text!) { (weather) in
            DispatchQueue.main.async {
                self.ClimaLabel.text = weather
            }
        }
    }
    

    
    

}
