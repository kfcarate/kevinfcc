//
//  Servicios.swift
//  App1
//
//  Created by kevin on 7/11/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import Foundation

class Servicios {
    func consultaPorCiudad (city:String, completion:@escaping (String) -> ()) {
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=f41020d6f457c1bf6a9ec1556b596a7b"
        consulta(urlStr: urlStr) { (weather, ciudad) in
            completion(weather)
        }
    }
    
    func consultaPorUbicacion (lat:Double, long:Double, completion:@escaping (String, String) -> ()) {
        
        //let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&appid=f41020d6f457c1bf6a9ec1556b596a7b"
        
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&appid=f41020d6f457c1bf6a9ec1556b596a7b"
        
        consulta(urlStr: urlStr) { (weather, ciudad) in
            print(ciudad)
            completion(weather,ciudad)
        }
    }
    
    func consulta (urlStr: String, completion:@escaping (String, String)-> ()) {
        
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                return
            }
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDic = weatherJson as!  NSDictionary
                guard let weatherKey = weatherDic["weather"] as? NSArray else {
                    completion("Ciudad no valida","error")
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                print(weatherDic)
                completion("\(weather["description"])","\(weatherDic["name"])")
            }
            catch{
                print("Error al generar el Json")
            }
        }
        task.resume()
        
    }
}
