//
//  InvitadoViewController.swift
//  App1
//
//  Created by kevin on 25/10/17.
//  Copyright © 2017 kevin. All rights reserved.
//

import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate{

    
    @IBOutlet weak var Clima1Label: UILabel!
    @IBOutlet weak var Ciudad1Label: UILabel!
    var bandera = false
    let locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- LocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let  location = manager.location?.coordinate
        //if !bandera {
            consultarPorUbicacion(
                lat: (location?.latitude)!,
                long: (location?.longitude)!
            )
//            bandera = true
//        }
        locationManager.stopUpdatingLocation()
        print(location)
    }

    private func consultarPorUbicacion(lat:Double, long:Double) {
        
        let service = Servicios()
        service.consultaPorUbicacion(lat: lat, long: long) { (clima, ciudad) in
            DispatchQueue.main.sync {
                self.Clima1Label.text = clima
                self.Ciudad1Label.text = ciudad
            }
        }
    }
    //@IBAction func botonSalir(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
    //}
}
